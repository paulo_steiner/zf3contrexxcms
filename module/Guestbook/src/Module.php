<?php
namespace Guestbook;

use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ModuleManager\Feature\ConfigProviderInterface;

class Module implements \Zend\ModuleManager\Feature\ConfigProviderInterface
{

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                Model\DB\EntryTable::class => function ($container) {
                    $tableGateway = $container->get(Model\EntryTableGateway::class);
                    return new Model\DB\EntryTable($tableGateway);
                },
                Model\EntryTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Entry());
                    return new TableGateway('guestbook_entry', $dbAdapter, null, $resultSetPrototype);
                }
            ]
        ];
    }
    public function getControllerConfig()
    {
        return [
            'factories' => [
                Controller\IndexController::class => function($container) {
                    return new Controller\IndexController(
                        $container->get(Model\DB\EntryTable::class)
                    );
                },
            ],
        ];
    }
}