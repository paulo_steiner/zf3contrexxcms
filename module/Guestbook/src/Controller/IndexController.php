<?php
namespace Guestbook\Controller;

use Guestbook\Model\DB\EntryTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends \Zend\Mvc\Controller\AbstractActionController
{
    // Add this property:
    private $table;

    // Add this constructor:
    public function __construct(EntryTable $table)
    {
        $this->table = $table;
    }

    public function indexAction()
    {
        return new ViewModel([
            'arrEntries' => $this->table->fetchAll()
        ]);
    }

    public function addAction()
    {}

    public function editAction()
    {}

    public function deleteAction()
    {}
}