<?php
namespace Guestbook\Model;

class Entry
{

    public $id;

    public $datetime;

    public $user;

    public $message;

    public function exchangeArray(array $data)
    {
        $this->id = ! empty($data['id']) ? $data['id'] : null;
        $this->datetime = ! empty($data['datetime']) ? $data['datetime'] : null;
        $this->user = ! empty($data['user']) ? $data['user'] : null;
        $this->message = ! empty($data['message']) ? $data['message'] : null;
    }
}