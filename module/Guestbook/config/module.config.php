<?php
namespace Guestbook;

return [
    'router' => [
        'routes' => [
            'guestbook' => [
                'type' => \Zend\Router\Http\Segment::class,
                'options' => [
                    'route' => '/guestbook[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+'
                    ],
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action' => 'index'
                    ]
                ]
            ]
        ]
    ],
    'view_manager' => [
        'template_path_stack' => [
            'album' => __DIR__ . '/../view'
        ]
    ]
];